package io.unravelling.carboncopy;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import com.palantir.docker.compose.DockerComposeRule;
import com.palantir.docker.compose.connection.DockerPort;
import com.palantir.docker.compose.connection.waiting.HealthChecks;

import com.tibco.tibjms.admin.QueueInfo;
import com.tibco.tibjms.admin.TibjmsAdmin;
import com.tibco.tibjms.admin.TibjmsAdminException;

import io.unravelling.carboncopy.config.CarbonCopyConfiguration;
import io.unravelling.carboncopy.factories.producers.ProducerFactory;
import io.unravelling.carboncopy.factories.workers.WorkerFactory;
import io.unravelling.carboncopy.producers.CarbonProducer;
import io.unravelling.carboncopy.workers.CarbonWorker;

import org.junit.BeforeClass;
import org.junit.ClassRule;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;


/**
 * Test class that tests copying messages from one ems to another.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@EnableConfigurationProperties(CarbonCopyConfiguration.class)
@TestPropertySource(locations = "classpath:config/application-ems2ems.yml")
@Ignore
public class CarbonEmsTest {

  /**
   * Default class logger.
   */
  private static final Logger log = LoggerFactory.getLogger(CarbonEmsTest.class);

  /**
   * Defines the name of the docker service that will be used to test.
   */
  private static final String TIBCOEMS_SERVICE = "tibcoems";

  /**
   * Port where to connect to TIBCO EMS.
   */
  private static final int TIBCOEMS_PORT = 7222;

  /**
   * Queue from where to read the messages from.
   */
  private static final String SOURCE_QUEUE = "io.unravelling.test.source";

  /**
   * Queue where to write the messages to.
   */
  private static final String TARGET_QUEUE = "io.unravelling.test.target";

  /**
   * Configuration to use.
   */
  @Autowired
  private CarbonCopyConfiguration configuration;

  /**
   * Class rule to load the Docker container to be used for testing.
   */
  @ClassRule
  public static final DockerComposeRule docker = DockerComposeRule.builder()
      .file("src/test/resources/docker/ems/docker-compose-tibco-ems.yml")
      .waitingForService(TIBCOEMS_SERVICE, HealthChecks.toHaveAllPortsOpen())
      .build();

  /**
   * Prepares the common resources that are not being tested.
   */
  @SuppressWarnings("unused")
  @BeforeClass
  public static void initialise() throws TibjmsAdminException {

    final DockerPort tibcoEms = docker.containers().container(TIBCOEMS_SERVICE)
        .port(TIBCOEMS_PORT);

    final String emsUrl = String.format("tcp://%s:%s",
        tibcoEms.getIp(), tibcoEms.getExternalPort());

    if (log.isInfoEnabled()) {
      log.info("EMS started and running as {}.", emsUrl);
    }

    prepareEms(emsUrl);

  }

  /**
   * Prepares the needed Ems resources.
   * @param url Url of the server where to prepare the resources.
   */
  private static void prepareEms(final String url) throws TibjmsAdminException {
    final TibjmsAdmin admin = new TibjmsAdmin(url, "admin","");

    final String source = SOURCE_QUEUE;
    final String target = TARGET_QUEUE;

    final QueueInfo sourceQueue = new QueueInfo(
        source
    );
    final QueueInfo targetQueue = new QueueInfo(
        target
    );

    admin.createQueue(sourceQueue);
    admin.createQueue(targetQueue);

    assertNotNull("Source queue was not created.",
        admin.getQueue(source));

    assertNotNull("Target queue was not created.",
        admin.getQueue(source));

    if (log.isInfoEnabled()) {
      log.info("Source and target queues created. We are ready to rock and roll.");
    }
  }

  /**
   * Initial dummy test.
   */
  @Test
  public void dummyTest() throws Exception {

    final CarbonCopyConfiguration.Copy copy = configuration.getCopy().get(0);

    final CarbonProducer producer = ProducerFactory.buildProducer(copy.getTarget());

    // Creates the worker that will receive the messages and process them.
    final CarbonWorker worker = WorkerFactory.buildWorker(configuration, copy, producer);
    worker.setProducer(producer);

    assertTrue(true);
  }
}
