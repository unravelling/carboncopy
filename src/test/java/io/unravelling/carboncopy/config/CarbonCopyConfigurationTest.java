package io.unravelling.carboncopy.config;

import static org.hamcrest.MatcherAssert.assertThat;

import org.junit.Before;
import org.junit.Test;

/**
 * Tests the carbon copy configuration.
 *
 * @author Tiago Veiga Lázaro
 * @version 1.0.0
 * @since 1.0.0
 */
public class CarbonCopyConfigurationTest {

  /**
   * Configuration used to test.
   */
  private CarbonCopyConfiguration carbonCopyConfiguration;

  /**
   * Prepares the dependencies for execution.
   */
  @Before
  public void setup() {
    this.carbonCopyConfiguration = new CarbonCopyConfiguration();
  }

  /**
   * Tests that the minthreads setting is correctly set.
   */
  @Test
  public void testMinthreads() {
    final int value = 10;
    this.carbonCopyConfiguration.setMinthreads(value);

    assertThat("The minimum amount of threads did not match.",
        Integer.valueOf(this.carbonCopyConfiguration.getMinthreads()).equals(value));
  }

  /**
   * Tests setting the max threads.
   */
  @Test
  public void testMaxthreads() {
    final int value = 10;
    this.carbonCopyConfiguration.setMaxthreads(value);

    assertThat("The maximum amount of threads did not match.",
        Integer.valueOf(this.carbonCopyConfiguration.getMaxthreads()).equals(value));
  }
}
