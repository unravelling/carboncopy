package io.unravelling.carboncopy.utils;

import org.junit.*;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import static org.junit.Assert.*;

/**
 * Tests the file utils methods.
 *
 * @author Tiago Veiga Lázaro
 * @version 2.5.0
 * @since 2.5.0
 */
public class FileUtilsTest {

  private static final String testBasePath = "/var/tmp";

  private static Path testFolder;

  @BeforeClass
  public static void prepare() {
    final DateFormat df = new SimpleDateFormat("yyyy.MM.dd");
    Date date = new Date();
    testFolder = Paths.get(testBasePath, df.format(date));
    FileUtils.checkPath(testFolder);
  }

  @AfterClass
  public static void tearDown() throws IOException {
    org.apache.commons.io.FileUtils.deleteDirectory(testFolder.toFile());
  }

  @Test
  public void checkPath() {
    assertTrue("Expected folder to exist", Files.exists(testFolder));
  }

  @Test
  public void testFileWrite() throws IOException {
    final String myContent = "abcdef123458";
    final String testFilename = "test.txt";

    FileUtils.writeStringFile(testBasePath, testFilename, myContent);

    final byte[] result = Files.readAllBytes(testFolder.resolve(testFilename));

    assertEquals("Expected strings to match.", myContent, new String(result, StandardCharsets.UTF_8));
  }
}
