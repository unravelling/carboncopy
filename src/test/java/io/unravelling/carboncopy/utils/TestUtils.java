package io.unravelling.carboncopy.utils;

import java.util.Arrays;
import java.util.Random;

/**
 * Test utilities.
 *
 * @author Tiago Veiga Lázaro
 * @version 2.5.0
 * @since 2.5.0
 */
public abstract class TestUtils {

  public static String generateRandomString(final int size) {
    Random r = new Random();
    char[] string = new char[size];

    for (int i = 0; i < size; i++) {
      string[i] = (char) (r.nextInt(26) + 'a');
    }

    return new String(string);
  }
}
