package io.unravelling.carboncopy.producers;

import static java.util.stream.Collectors.toList;
import static org.apache.commons.io.FileUtils.deleteDirectory;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import com.salesforce.kafka.test.KafkaTestUtils;
import com.salesforce.kafka.test.junit4.SharedKafkaTestResource;

import io.unravelling.carboncopy.config.CarbonCopyConfiguration;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Clock;
import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.stream.Stream;

import io.unravelling.carboncopy.utils.TestUtils;
import org.apache.kafka.clients.admin.TopicDescription;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;

import org.apache.kafka.common.Node;
import org.apache.kafka.common.PartitionInfo;
import org.apache.kafka.common.TopicPartition;
import org.apache.kafka.common.serialization.StringDeserializer;

import org.junit.After;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Tests for the Kafka producer.
 *
 * @author Tiago Veiga Lázaro
 * @version 2.5.0
 * @since 2.5.0
 */
public class KafkaProducerTest {

 private static final Logger log = LoggerFactory.getLogger(KafkaProducerTest.class);

 private String topicName;

 private String dumpPath = "/var/tmp";

  /**
   * Prepares the kafka server used for the tests.
   */
  @ClassRule
  public static final SharedKafkaTestResource sharedKafkaTestResource = new SharedKafkaTestResource()
      .withBrokerProperty("auto.create.topics.enable", "false")
      .withBrokerProperty("message.max.bytes", "128");

  /**
   * Prepares the needed resources for the tests (e.g., creating the topic used for each test)
   */
  @Before
  public void prepare() {
    // Generate topic name
    topicName = getClass().getSimpleName() + Clock.systemUTC().millis();

    // Create topic with 3 partitions,
    // NOTE: This will create partition ids 0 thru 2, because partitions are indexed at 0 :)
    getKafkaTestUtils().createTopic(topicName, 3, (short) 1);
  }

  /**
   * Cleans up after each test.
   *
   * @throws IOException Exception while deleting the work directory.
   */
  @After
  public void tearDown() throws IOException {
    final DateFormat df = new SimpleDateFormat("yyyy.MM.dd");
    Date date = new Date();
    Path testFolder = Paths.get(dumpPath, df.format(date));

    if (Files.isDirectory(testFolder)) {
      deleteDirectory(testFolder.toFile());
    }
  }

  @Test
  public void testTopicIsCorrect() {
    final TopicDescription topicDescription = getKafkaTestUtils().describeTopic(topicName);
    assertNotNull("Should return a result", topicDescription);

    // Debug logging.
    log.info("Found topic with name {} and {} partitions.", topicDescription.name(), topicDescription.partitions().size());

    assertEquals("Topic should have 3 partitions", 3, topicDescription.partitions().size() );
    assertFalse("Our topic is not an internal topic", topicDescription.isInternal());
    assertEquals("Has the correct name", topicName, topicDescription.name());
  }

  @Test
  public void testSimpleKafkaProducer() {
    final String sampleMessage = "sample";

    final KafkaProducer kafkaProducer = new KafkaProducer(prepareConfig());
    kafkaProducer.send(sampleMessage);

    String result = consumeMessage();

    assertEquals("Expected message to match", sampleMessage, result);
  }

  @Test
  public void testDumpedMessageTooBigOnProducer() throws IOException {
    final String bigMessage = TestUtils.generateRandomString(512);

    final CarbonCopyConfiguration.ConnectionConfiguration configuration = prepareConfig();
    configuration.setDumpDroppedMessages(true);
    configuration.setDumpedMessagesPath(dumpPath);
    final KafkaProducer kafkaProducer = new KafkaProducer(configuration);
    kafkaProducer.send(bigMessage);

    String result = consumeMessage();

    assertEquals("Expected message to match", "", result);

    final DateFormat df = new SimpleDateFormat("yyyy.MM.dd");
    Date date = new Date();
    Path testDir = Paths.get(dumpPath, df.format(date));
    assertTrue("Expected dump directory to exist", Files.isDirectory(testDir));

    List<Path> dumpedFiles = Files.list(testDir).collect(toList());


    assertEquals("Expected one dumped message", 1, dumpedFiles.size());
    Path dumpedFile = dumpedFiles.get(0);
    assertNotNull(dumpedFile);

    final byte[] resultContents = Files.readAllBytes(dumpedFile);

    assertEquals("Expected strings to match.", bigMessage, new String(resultContents, StandardCharsets.UTF_8));
  }

  private String consumeMessage() {
    // Create consumer
    Properties config = new Properties();
    config.put("max.poll.records", 100);

    try (final KafkaConsumer<String, String> kafkaConsumer =
             getKafkaTestUtils().getKafkaConsumer(StringDeserializer.class, StringDeserializer.class, config)) {

      final List<TopicPartition> topicPartitionList = new ArrayList<>();
      for (final PartitionInfo partitionInfo: kafkaConsumer.partitionsFor(topicName)) {
        topicPartitionList.add(new TopicPartition(partitionInfo.topic(), partitionInfo.partition()));
      }
      kafkaConsumer.assign(topicPartitionList);
      kafkaConsumer.seekToBeginning(topicPartitionList);

      // Pull records from kafka, keep polling until we get nothing back
      ConsumerRecords<String, String> records;
      records = kafkaConsumer.poll(Duration.of(100, ChronoUnit.MILLIS));
      String result = "";

      for (ConsumerRecord<String, String> record : records) {
        result = record.value();
      }
      log.info("Found {} records in kafka", records.count());
      return result;
    }
  }

  private CarbonCopyConfiguration.ConnectionConfiguration prepareConfig() {
    final CarbonCopyConfiguration.ConnectionConfiguration configuration =
        new CarbonCopyConfiguration.ConnectionConfiguration();

    final Collection<Node> nodes = getKafkaTestUtils().describeClusterNodes();
    final String hostname = ((Node)nodes.toArray()[0]).host();
    final int port = ((Node)nodes.toArray()[0]).port();

    configuration.setUrl(String.format("%s:%d", hostname, port));
    configuration.setDestination(topicName);

    return configuration;
  }

  private KafkaTestUtils getKafkaTestUtils() {
    return sharedKafkaTestResource.getKafkaTestUtils();
  }
}
