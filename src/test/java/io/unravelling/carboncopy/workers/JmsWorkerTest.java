package io.unravelling.carboncopy.workers;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import io.unravelling.carboncopy.config.CarbonCopyConfiguration;
import io.unravelling.carboncopy.utils.TestUtils;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.jms.JMSException;
import javax.jms.TextMessage;

import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.Test;

/**
 * Tests the JMS Worker.
 *
 * @author Tiago Veiga Lázaro
 * @version 2.5.0
 * @since 2.5.0
 */
public class JmsWorkerTest {

  private static final DateFormat df = new SimpleDateFormat("yyyy.MM.dd");

  private static final String dumpPath = "/var/tmp";
  private static final String messageId = "messageID";

  @After
  public void tearDown() throws IOException {
    Path testDir = Paths.get(dumpPath, df.format(new Date()));
    if (Files.isDirectory(testDir)) {
      FileUtils.deleteDirectory(testDir.toFile());
    }
  }

  @Test
  public void testReceiveTooBigMessage() throws JMSException, IOException {

    final String messageContent = TestUtils.generateRandomString(512);

    final CarbonCopyConfiguration.ConnectionConfiguration sourceConfiguration
        = new CarbonCopyConfiguration.ConnectionConfiguration();
    sourceConfiguration.setMessageSizeThreshold(128);
    sourceConfiguration.setDumpDroppedMessages(true);
    sourceConfiguration.setDumpedMessagesPath(dumpPath);

    JmsWorker worker = new JmsWorker(sourceConfiguration);

    TextMessage message = mock(TextMessage.class);
    when(message.getJMSMessageID()).thenReturn(messageId);
    when(message.getText()).thenReturn(messageContent);

    worker.onMessage(message, null);

    Date date = new Date();
    Path testFolder = Paths.get(dumpPath, df.format(date));
    final byte[] result = Files.readAllBytes(testFolder.resolve(messageId));

    assertEquals("Expected strings to match.", messageContent, new String(result, StandardCharsets.UTF_8));
  }
}
