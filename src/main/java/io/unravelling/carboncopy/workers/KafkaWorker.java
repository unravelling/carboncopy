/*
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * Contributors:
 * Tiago Veiga Lázaro
 */

package io.unravelling.carboncopy.workers;

import io.unravelling.carboncopy.config.CarbonCopyConfiguration.ConnectionConfiguration;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.listener.AcknowledgingMessageListener;
import org.springframework.kafka.listener.ConcurrentMessageListenerContainer;
import org.springframework.kafka.support.Acknowledgment;

/**
 * Class that implements a worker that picks up messages from a Kafka instance and sends them
 * to the defined target.
 *
 * @author Tiago Veiga Lázaro
 * @version 2.4.0
 * @since 2.4.0
 */
public class KafkaWorker<K,V> extends GenericWorker implements AcknowledgingMessageListener<K,V> {

  /**
   * Container used to read messages.
   */
  private ConcurrentMessageListenerContainer container;

  /**
   * Worker constructor.
   *
   * @param sourceConfiguration Configuration of the source.
   */
  public KafkaWorker(final ConnectionConfiguration sourceConfiguration) {
    super(sourceConfiguration);
  }

  /**
   * Gets the container.
   *
   * @return ConcurrentMessageListenerContainer.
   */
  private ConcurrentMessageListenerContainer getContainer() {
    return this.container;
  }

  /**
   * Sets the container.
   *
   * @param container ConcurrentMessageListenerContainer.
   */
  public void setContainer(final ConcurrentMessageListenerContainer container) {
    this.container = container;
  }

  /**
   * Invoked with data from kafka.
   *
   * @param data           the data to be processed.
   * @param acknowledgment the acknowledgment.
   */
  @Override
  public void onMessage(final ConsumerRecord<K, V> data, final Acknowledgment acknowledgment) {
    log.debug("Message received.");

    this.registerReceivedMessage();

    this.producer.send(data.value().toString());

    this.registerSentMessage();

    log.debug("Message sent.");
  }

  /**
   * Overrides the close method.
   */
  @Override
  public void close() {
    log.info("Closing all resources.");

    super.close();

    if (this.container != null) {
      log.info("Stopping container...");
      this.container.stop();
    }
  }
}