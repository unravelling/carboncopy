/*
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * Contributors:
 * Tiago Veiga Lázaro
 */

package io.unravelling.carboncopy.workers;

import io.unravelling.carboncopy.config.CarbonCopyConfiguration.ConnectionConfiguration;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Session;
import javax.jms.TextMessage;

import io.unravelling.carboncopy.utils.FileUtils;
import org.springframework.jms.listener.SessionAwareMessageListener;

import java.nio.charset.StandardCharsets;

/**
 * <p>Worker to process Ems messages.</p>
 *
 * <p>Created on 2017/04/21.</p>
 *
 * @author Tiago Veiga Lázaro
 * @version 1.0.0
 */
public class JmsWorker extends GenericWorker implements SessionAwareMessageListener, CarbonWorker {

  /**
   * Constructor of the JMS Worker.
   *
   * @param sourceConfiguration Configuration of the source to implement.
   */
  public JmsWorker(final ConnectionConfiguration sourceConfiguration) {
    super(sourceConfiguration);
  }

  /**
   * Message receiver.
   *
   * @param message Message that was read from the queue.
   * @param session Session over which the message was read.
   * @throws JMSException Exception while reading the message.
   */
  @Override
  public void onMessage(final Message message, final Session session) throws JMSException {
    log.debug("Read message {} from the source.", message.getJMSMessageID());

    this.registerReceivedMessage();

    if (message instanceof TextMessage && sourceConfiguration.isDumpDroppedMessages() && sourceConfiguration.getMessageSizeThreshold() > 0) {
      final String str = ((TextMessage) message).getText();
      if (str.getBytes(StandardCharsets.UTF_8).length > sourceConfiguration.getMessageSizeThreshold()) {
        this.registerDroppedMessage();
        log.debug("Dropping message {} due to crossing size threshold.", message.getJMSMessageID());
        FileUtils.writeStringFile(sourceConfiguration.getDumpedMessagesPath(),
            message.getJMSMessageID(),
            str);
        return;
      }
    }

    if (getProducer() == null) {
      throw new JMSException("Producer was not initialised and message cannot be sent.");
    }

    producer.send(message);

    log.debug("Message sent to target with new id {}.", message.getJMSMessageID());

    this.registerSentMessage();
  }
}
