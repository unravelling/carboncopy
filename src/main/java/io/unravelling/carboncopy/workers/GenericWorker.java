package io.unravelling.carboncopy.workers;

import io.prometheus.client.Counter;

import io.unravelling.carboncopy.CarbonCopy;
import io.unravelling.carboncopy.config.CarbonCopyConfiguration.ConnectionConfiguration;
import io.unravelling.carboncopy.producers.CarbonProducer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Implementation of the generic worker that other classes will be able to extend to reuse the code.
 *
 * @author Tiago Veiga Lázaro
 * @version 2.4.0
 * @since 2.4.0
 */
@SuppressWarnings("WeakerAccess")
public class GenericWorker implements CarbonWorker {

  private static final String GENERIC = "generic";

  /**
   * Class logger.
   */
  protected final Logger log = LoggerFactory.getLogger(this.getClass().getName());

  /**
   * Source configuration. Used to get the data for the metrics.
   */
  protected ConnectionConfiguration sourceConfiguration;

  /**
   * Producer where to send the messages to.
   */
  protected CarbonProducer producer;

  /**
   * Counter that keeps track of received messages.
   */
  protected static final Counter receivedMessages = CarbonCopy.receivedMessages;

  /**
   * Counter that keeps track of successfully sent messages.
   */
  protected static final Counter sentMessages = CarbonCopy.sentMessages;

  /**
   * Counter that keeps track of the dropped messages.
   */
  protected static final Counter droppedMessages = CarbonCopy.droppedMessages;

  /**
   * Constructor of the class supplying the source configuration.
   *
   * @param sourceConfiguration Configuration of the source.
   */
  public GenericWorker(final ConnectionConfiguration sourceConfiguration) {
    this.sourceConfiguration = sourceConfiguration;
  }

  /**
   * Producer where to send the messages to.
   */
  protected CarbonProducer getProducer() {
    return producer;
  }

  /**
   * Sets the producer of this worker.
   *
   * @param producer Producer which sends messages into a target.
   */
  @Override
  public void setProducer(final CarbonProducer producer) {
    this.producer = producer;
  }

  /**
   * Registers a received message.
   */
  protected void registerReceivedMessage() {
    final String type = this.sourceConfiguration.getType();
    final String destinationType = this.sourceConfiguration.getDestinationType();
    final String destination = this.sourceConfiguration.getDestination();

    receivedMessages.labels(
        type != null ? type.toLowerCase() : GENERIC,
        destinationType != null ? destinationType.toLowerCase() : GENERIC,
        destination != null ? destination.toLowerCase() : GENERIC
    ).inc();
  }

  /**
   * Registers a sent message.
   */
  protected void registerSentMessage() {
    final String type = getProducer().getType();
    final String destinationType = getProducer().getDestinationType();
    final String destination = getProducer().getDestinationName();

    sentMessages.labels(
        type != null ? type.toLowerCase() : GENERIC,
        destinationType != null ? destinationType.toLowerCase() : GENERIC,
        destination != null ? destination.toLowerCase() : GENERIC
    ).inc();
  }

  /**
   * Registers a received message.
   */
  protected void registerDroppedMessage() {
    final String type = this.sourceConfiguration.getType();
    final String destinationType = this.sourceConfiguration.getDestinationType();
    final String destination = this.sourceConfiguration.getDestination();

    droppedMessages.labels(
        type != null ? type.toLowerCase() : GENERIC,
        destinationType != null ? destinationType.toLowerCase() : GENERIC,
        destination != null ? destination.toLowerCase() : GENERIC
    ).inc();
  }

  /**
   * Closes the pending resources.
   */
  @Override
  public void close() {
    if (producer != null) {
      producer.close();
    }
  }
}
