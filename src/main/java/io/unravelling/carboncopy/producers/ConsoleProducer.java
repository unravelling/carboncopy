package io.unravelling.carboncopy.producers;

import javax.jms.JMSException;
import javax.jms.Message;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Simple producer that forwards the received messages into the console (via logger).
 *
 * @author Tiago Veiga Lázaro
 * @version 1.4.0
 * @since 1.4.0
 */
public class ConsoleProducer implements CarbonProducer {

  /**
   * Class logger.
   */
  private static final Logger log = LoggerFactory.getLogger(ConsoleProducer.class);

  /**
   * Sends a message to the target. A JMS format message must be supported.
   *
   * @param message Message to be sent.
   */
  @Override
  public void send(final String message) {
    log.info(message);
  }

  /**
   * Sends a jms message.
   *
   * @param message Message to be sent.
   */
  @Override
  public void send(final Message message) {
    try {
      log.info(message.getBody(Object.class).toString());
    } catch (final JMSException jmsException) {
      log.error("Error occurred while reading message.", jmsException);
    }
  }

  /**
   * Closes the pending resources, such as connections.
   */
  @Override
  public void close() {
    // Nothing to do.
  }

  /**
   * Gets the type of producer.
   *
   * @return Type of producer.
   */
  @Override
  public String getType() {
    return null;
  }

  /**
   * Gets the type of the destination.
   *
   * @return Type of destination.
   */
  @Override
  public String getDestinationType() {
    return null;
  }

  /**
   * Gets the name of the destination.
   *
   * @return Name of the destination.
   */
  @Override
  public String getDestinationName() {
    return null;
  }
}
