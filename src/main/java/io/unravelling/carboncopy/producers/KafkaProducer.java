package io.unravelling.carboncopy.producers;

import io.unravelling.carboncopy.config.CarbonCopyConfiguration.ConnectionConfiguration;
import io.unravelling.carboncopy.config.CarbonCopyKafkaProducerListener;
import io.unravelling.carboncopy.factories.connections.KafkaConnectionFactory;

import javax.jms.JMSException;
import javax.jms.Message;

import org.springframework.kafka.core.KafkaTemplate;

/**
 * Producer for Kafka targets.
 *
 * @author Tiago Veiga Lázaro
 * @version 2.4.0
 * @since 2.4.0
 */
public class KafkaProducer extends GenericProducer implements CarbonProducer {

  /**
   * Template used to send messages to Kafka.
   */
  private final KafkaTemplate<String, String> kafkaTemplate;

  /**
   * Constructor of the producer.
   *
   * @param connectionConfiguration Connection configuration to use to build the producer.
   */
  public KafkaProducer(final ConnectionConfiguration connectionConfiguration) {
    super(connectionConfiguration);

    this.kafkaTemplate = KafkaConnectionFactory.getKafkaTemplate(connectionConfiguration);
    kafkaTemplate.setProducerListener(new CarbonCopyKafkaProducerListener<String, String>(connectionConfiguration));
  }

  /**
   * Sends a message to the target. A string message must be supported.
   *
   * @param message Message to be sent.
   */
  @Override
  public void send(final String message) {
    log.debug("Sending message to topic {}...", getDestinationName());

    this.kafkaTemplate.send(getDestinationName(), String.valueOf(System.currentTimeMillis()), message);

    log.debug("Text message sent.");
  }

  /**
   * Sends a message to the target. A JMS format message must be supported.
   *
   * @param message Message to be sent.
   */
  @Override
  public void send(final Message message) {
    log.debug("Sending message to topic {}...", getDestinationName());

    try {
      this.kafkaTemplate.send(getDestinationName(), message.getJMSMessageID(), message.getBody(String.class));
      message.acknowledge();
    } catch (JMSException jmsException) {
      log.error("Error while opening the JMS message.", jmsException);
    }

    log.debug("Original JMS message sent to Kafka and confirmed in JMS.");
  }

  /**
   * Closes the pending resources, such as connections.
   */
  @Override
  public void close() {
    // do nothing
  }
}
