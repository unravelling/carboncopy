/*
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * Contributors:
 * Tiago Veiga Lázaro
 */

package io.unravelling.carboncopy.producers;

import javax.jms.Message;

/**
 * Defines Carbon Copy's producers.
 *
 * @author Tiago Veiga Lazaro
 * @version 2.4.0
 * @since 1.0.0
 */
public interface CarbonProducer {

  /**
   * Sends a message to the target. A string message must be supported.
   *
   * @param message Message to be sent.
   */
  void send(final String message);

  /**
   * Sends a message to the target. A JMS format message must be supported.
   * @param message Message to be sent.
   */
  void send(final Message message);

  /**
   * Closes the pending resources, such as connections.
   */
  void close();

  /**
   * Gets the type of producer.
   * @return Type of producer.
   */
  String getType();

  /**
   * Gets the type of the destination.
   * @return Type of destination.
   */
  String getDestinationType();

  /**
   * Gets the name of the destination.
   * @return Name of the destination.
   */
  String getDestinationName();
}
