/*
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * Contributors:
 * Tiago Veiga Lázaro
 */

package io.unravelling.carboncopy.producers;

import io.unravelling.carboncopy.config.CarbonCopyConfiguration.ConnectionConfiguration;

import java.io.IOException;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.TextMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * <p>Implements a producer for writing read messages into files.</p>
 *
 * <p>Created on 2017/04/26</p>
 * @author Tiago Veiga Lázaro
 * @version 1.0.0
 */
public class FileProducer implements CarbonProducer {

  /**
   * Class logger.
   */
  private static final Logger log = LoggerFactory.getLogger(FileProducer.class);

  /**
   * Default enconding to use.
   */
  private static final String DEFAULT_ENCODING = "UTF-8";

  /**
   * Path of the folder where to write the messages to.
   */
  private String path;

  /**
   * Default constructor for the producer.
   * @param connectionConfiguration Configuration of the target connection.
   */
  public FileProducer(final ConnectionConfiguration connectionConfiguration) {
    setPath(connectionConfiguration.getUrl());

    log.info("Created new file producer writing messages to {}.", getPath());
  }

  /**
   * Sends a String message.
   *
   * @param message Message to be sent.
   */
  @Override
  public void send(final String message) {

    final Path filePath = Paths.get(getPath().concat(LocalDateTime.now().toString()));

    try {
      Files.write(filePath, message.getBytes(DEFAULT_ENCODING));
    } catch (IOException ioException) {
      log.error("Unable to write message to the filesystem.", ioException);
    }
  }

  /**
   * Sends a message into the target.
   * @param message Message to be sent.
   */
  @Override
  public void send(final Message message) {
    try {

      if (!(message instanceof TextMessage)) {
        throw new UnsupportedOperationException("Message is not of type TextMessage");
      }

      final Path filePath = Paths.get(getPath().concat(
          sanitizeMessageId(message.getJMSMessageID())));

      Files.write(filePath, ((TextMessage) message).getText().getBytes(DEFAULT_ENCODING));

    } catch (final JMSException jmsException) {
      log.error("Unable to process message.", jmsException);
    } catch (final UnsupportedOperationException unsupportedException) {
      log.error("Unable to process message, as it does not have a supported type.",
          unsupportedException);
    } catch (IOException ioException) {
      log.error("Unable to write message to the filesystem.", ioException);
    }
  }

  /**
   * Closes the pending resources opened by this producer.
   */
  @Override
  public void close() {
    log.info("Nothing to be closed.");
  }

  /**
   * Gets the type of producer.
   *
   * @return Type of producer.
   */
  @Override
  public String getType() {
    return "file";
  }

  /**
   * Gets the type of the destination.
   *
   * @return Type of destination.
   */
  @Override
  public String getDestinationType() {
    return "file";
  }

  /**
   * Gets the name of the destination.
   */
  @Override
  public String getDestinationName() {
    return this.path;
  }

  /**
   * Path of the folder where to write the messages to.
   */
  private String getPath() {
    return path;
  }

  /**
   * Sets the path to the folder where to keep the messages in.
   * @param targetPath Path to the target folder.
   */
  private void setPath(final String targetPath) {

    if (targetPath.endsWith("/")) {
      this.path = targetPath;
    } else {
      this.path = targetPath.concat("/");
    }
  }

  /**
   * Sanitizes the message id so that it does not contain special characters.
   * @param messageid Message id to be sanitized.
   * @return Sanitized message id.
   */
  private String sanitizeMessageId(final String messageid) {
    return messageid.replaceAll("[^\\w\\d]", "");
  }
}
