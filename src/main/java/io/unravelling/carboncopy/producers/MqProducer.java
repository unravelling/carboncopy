/*
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * Contributors:
 * Tiago Veiga Lázaro
 */

package io.unravelling.carboncopy.producers;

import io.unravelling.carboncopy.config.CarbonCopyConfiguration.ConnectionConfiguration;

import javax.jms.ConnectionFactory;
import javax.jms.Message;
import javax.jms.Session;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jms.core.JmsTemplate;

/**
 * <p>Handles the sending of messages into a Tibco EMS instance.</p>
 *
 * <p>Created on 2017/04/21.</p>
 * @author Tiago Veiga Lázaro.
 * @version 1.0.0
 */
public class MqProducer extends GenericProducer implements CarbonProducer {

  /**
   * Class logger.
   */
  private final Logger log = LoggerFactory.getLogger(MqProducer.class);

  /**
   * Template to be used to send the messages.
   */
  private final JmsTemplate jmsTemplate;

  /**
   * Creates an instance of this producer.
   * @param connectionFactory Connection factory to use for sending the messages.
   */
  public MqProducer(final ConnectionFactory connectionFactory,
                    final ConnectionConfiguration connectionConfiguration) {

    super(connectionConfiguration);

    jmsTemplate = new JmsTemplate(connectionFactory);
    jmsTemplate.setSessionAcknowledgeMode(Session.CLIENT_ACKNOWLEDGE);

    if ("topic".equalsIgnoreCase(connectionConfiguration.getDestinationType())) {
      jmsTemplate.setPubSubDomain(true);
    }

    log.info("Producer created and initialised.");
  }

  /**
   * Sends a String based message.
   *
   * @param message Message to be sent.
   */
  @Override
  public void send(final String message) {
    jmsTemplate.convertAndSend(destinationName, message);
  }

  /**
   * Sends a message.
   * @param message Message to be sent.
   */
  @Override
  public void send(final Message message) {

    jmsTemplate.send(destinationName, session -> message);

    log.debug("Message sent to target.");
  }

  /**
   * Closes the pending connections.
   */
  @Override
  public void close() {
    log.info("Closing pending connection on the jmsTemplate.");
  }
}
