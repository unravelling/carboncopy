package io.unravelling.carboncopy.producers;

import io.unravelling.carboncopy.config.CarbonCopyConfiguration.ConnectionConfiguration;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Producer which contains the generic properties all producers share.
 *
 * @author Tiago Veiga Lázaro
 * @version 2.4.0
 * @since 2.4.0
 */
public class GenericProducer {

  /**
   * Class logger.
   */
  protected final Logger log = LoggerFactory.getLogger(this.getClass());

  /**
   * Type of the producer.
   */
  protected String type;

  /**
   * Name of the destination where to send the message to.
   */
  @SuppressWarnings("WeakerAccess")
  protected String destinationName;

  /**
   * Type of the destination (queue or topic).
   */
  protected String destinationType;

  /**
   * Default constructor.
   *
   * @param connectionConfiguration Configuration to be used for the producer.
   */
  public GenericProducer(final ConnectionConfiguration connectionConfiguration) {
    this.type = connectionConfiguration.getType();
    this.destinationName = connectionConfiguration.getDestination();
    this.destinationType = connectionConfiguration.getDestinationType();

    log.info("Creating a new {} producer.", this.type);
  }

  /**
   * Gets the type of producer.
   *
   * @return Type of producer.
   */
  public String getType() {
    return this.type;
  }

  /**
   * Gets the type of the destination.
   *
   * @return Type of destination.
   */
  public String getDestinationType() {
    return this.destinationType;
  }

  /**
   * Gets the name of the destination.
   *
   * @return Name of the destination.
   */
  public String getDestinationName() {
    return this.destinationName;
  }
}
