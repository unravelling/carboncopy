/*
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * Contributors:
 * Tiago Veiga Lázaro
 */

package io.unravelling.carboncopy.config;

import io.prometheus.client.CollectorRegistry;
import io.prometheus.client.exporter.MetricsServlet;
import io.prometheus.client.hotspot.DefaultExports;
import io.prometheus.client.spring.boot.SpringBootMetricsCollector;

import java.util.Collection;

import org.springframework.boot.actuate.endpoint.PublicMetrics;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Class that implements the Prometheus endpoint for metrics collection.
 *
 * @author Tiasgo Veiga Lázaro
 * @version 2.0.0
 * @since 2.0.0
 */
@Configuration
public class PrometheusConfiguration {

  /**
   * Gets a metrics collector.
   *
   * @param publicMetrics  Metrics to be collected.
   * @return SpringBootMetricsCollector.
   */
  @Bean
  public SpringBootMetricsCollector springBootMetricsCollector(
      final Collection<PublicMetrics> publicMetrics) {

    SpringBootMetricsCollector springBootMetricsCollector =
        new SpringBootMetricsCollector(publicMetrics);
    springBootMetricsCollector.register();

    return springBootMetricsCollector;
  }

  /**
   * Registers the endpoint on which the metrics will be served.
   *
   * @return ServletRegistrationBean.
   */
  @Bean
  public ServletRegistrationBean servletRegistrationBean() {
    DefaultExports.initialize();
    return new ServletRegistrationBean(new MetricsServlet(), "/prometheus");
  }

  /**
   * Gets the collector registry to use.
   *
   * @return CollectorRegistry.
   */
  @Bean
  public CollectorRegistry collectorRegistry() {
    return new CollectorRegistry();
  }
}
