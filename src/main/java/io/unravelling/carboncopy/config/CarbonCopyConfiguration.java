/*
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * Contributors:
 * Tiago Veiga Lázaro
 */

package io.unravelling.carboncopy.config;

import io.unravelling.carboncopy.exception.InvalidConfigurationException;
import io.unravelling.carboncopy.model.CopyTypeEnum;

import java.util.List;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * Configuration of the CarbonCopy application.
 * @author Tiago Veiga Lazaro
 */
@SuppressWarnings("unused")
@ConfigurationProperties(prefix = "carbon")
public class CarbonCopyConfiguration {

  /**
   * Logger of the class.
   */
  private static final Logger LOGGER = LoggerFactory.getLogger(CarbonCopyConfiguration.class);

  /**
   * Minimum number of threads that will be spinned.
   */
  private int minthreads;

  /**
   * Max number of threads that will be used by the configuration.
   */
  private int maxthreads;

  /**
   * Configuration of a copy.
   */
  private List<Copy> copy;

  /**
   * Configuration of a copy.
   */
  public List<Copy> getCopy() {
    return copy;
  }

  /**
   * Sets the copy configuration.
   * @param copy the copy configuration to be set.
   */
  public void setCopy(final List<Copy> copy) {
    this.copy = copy;
  }

  /**
   * Minimum number of threads that will be spinned.
   */
  public int getMinthreads() {
    return minthreads;
  }

  /**
   * Sets the minimum threads to run.
   * @param minthreads number of threads.
   */
  public void setMinthreads(final int minthreads) {
    this.minthreads = minthreads;
  }

  /**
   * Max number of threads that will be used by the configuration.
   */
  public int getMaxthreads() {
    return maxthreads;
  }

  /**
   * Max number of threads to run.
   * @param maxthreads number of threads.
   */
  public void setMaxthreads(final int maxthreads) {
    this.maxthreads = maxthreads;
  }

  /**
   * Defines a copy element.
   */
  public static class Copy {
    /**
     * Name of the copy.
     */
    private String name;

    /**
     * Description of the copy.
     */
    private String description;

    /**
     * Type of the copy to be set.
     */
    private CopyTypeEnum type;

    /**
     * A source connection configuration.
     */
    private ConnectionConfiguration source;

    /**
     * A target connection configuration.
     */
    private ConnectionConfiguration target;

    /**
     * Name of the copy.
     */
    public String getName() {
      return name;
    }

    /**
     * Sets the name of the copy.
     * @param name Name of the copy.
     */
    public void setName(final String name) {
      this.name = name;
    }

    /**
     * Type of the copy to be set.
     */
    public CopyTypeEnum getType() {
      return type;
    }

    /**
     * Sets the type of the copy.
     * @param type of the copy.
     */
    public void setType(final CopyTypeEnum type) {
      this.type = type;
    }

    /**
     * Gets the source connection configuration.
     * @return ConnectionConfiguration.
     */
    public ConnectionConfiguration getSource() {
      return source;
    }

    /**
     * Sets the source configuration.
     * @param source configuration.
     */
    public void setSource(final ConnectionConfiguration source) {
      this.source = source;
    }

    /**
     * Gets the target connection configuration.
     * @return ConnectionConfiguration.
     */
    public ConnectionConfiguration getTarget() {
      return target;
    }

    public void setTarget(final ConnectionConfiguration target) {
      this.target = target;
    }

    /**
     * Description of the copy.
     */
    public String getDescription() {
      return description;
    }

    public void setDescription(final String description) {
      this.description = description;
    }
  }

  /**
   * Class that defines the configuration of a connection configuration.
   */
  public static class ConnectionConfiguration {
    /**
     * Type of the connection.
     */
    private String type;

    /**
     * Url of the connection.
     */
    private String url;

    /**
     * Queue or connection factory to use.
     */
    private String factory;

    /**
     * Username to use in the connection.
     */
    private String username;

    /**
     * Password to use in the connection.
     */
    private String password;

    /**
     * Destination to use for sending or receiving the messages.
     */
    private String destination;

    /**
     * Name of the consumer.
     */
    private String consumerName;

    /**
     * Type of the destination to implement (queue or topic).
     */
    private String destinationType;

    /**
     * Flag to indicate whether the topic subscriber should be durable.
     */
    private Boolean durable;

    /**
     * Name of the durable.
     */
    private String durablename;

    /**
     * Flag that indicates whether ssl is enabled for this connection or not.
     */
    private Boolean ssl;

    /**
     * Path to the trusted certificates.
     */
    private String trustedcertificates;

    /**
     * Flag which indicates if the host validation should be done.
     */
    private Boolean sslhost;

    /**
     * Flag which indicates the ssl verification of the hostname should be validated.
     */
    private Boolean sslhostname;

    /**
     * Enables or disables the ssl trace.
     */
    private Boolean ssltrace;

    /**
     * Flag which indicates that the ssl trace should be in debug mode.
     */
    private Boolean ssldebugtrace;

    /**
     * Cipher to use in the SSL connection.
     */
    private String sslcipher;

    /**
     * Path to the SSL certificates to use.
     */
    private String sslkeystore;

    /**
     * Password for the ssl keystore.
     */
    private String sslkeystorepassword;

    /**
     * Threshold in bytes, over which messages are just dropped. Set to 0 to not apply.
     */
    private long messageSizeThreshold;

    /**
     * Flag to check if dropped messages should be dumped into the filesystem or not.
     */
    private boolean dumpDroppedMessages;

    /**
     * Path where to save the dumped messages.
     */
    private String dumpedMessagesPath;

    /**
     * Gets the type of the connection.
     * @return Type.
     */
    public String getType() {
      return type;
    }

    /**
     * Sets the type of the server.
     * @param type Server type.
     */
    public void setType(final String type) {
      this.type = type;
    }

    /**
     * Gets the url where to connect.
     * @return Url where to connect to.
     */
    public String getUrl() {
      return url;
    }

    /**
     * Sets the url where to connect.
     * @param url URL.
     */
    public void setUrl(final String url) {
      this.url = url;
    }

    /**
     * Gets the username where to connect to.
     * @return Username.
     */
    public String getUsername() {
      return username;
    }

    /**
     * Sets the username.
     * @param username Username.
     */
    public void setUsername(final String username) {
      this.username = username;
    }

    /**
     * Password to use in the connection.
     * @return Password.
     */
    public String getPassword() {
      return password;
    }

    /**
     * Sets the password.
     * @param password Password.
     */
    public void setPassword(final String password) {
      this.password = password;
    }

    /**
     * Destination where to send or receive the messages.
     * @return Destination.
     */
    public String getDestination() {
      return destination;
    }

    /**
     * Sets the destination name.
     * @param destination Destination name.
     */
    public void setDestination(final String destination) {
      this.destination = destination;
    }

    /**
     * Gets the name of the consumer.
     *
     * @return String name of the consumer.
     */
    public String getConsumerName() {
      return consumerName;
    }

    /**
     * Sets the name of the consumer.
     *
     * @param consumerName Name of the consumer.
     */
    public void setConsumerName(final String consumerName) {
      this.consumerName = consumerName;
    }

    /**
     * Gets the type of the destination (queue or topic).
     * @return Type of the destination.
     */
    public String getDestinationType() {
      return destinationType;
    }

    /**
     * Type of the destination.
     * @param destinationType Type of the destination.
     */
    public void setDestinationType(final String destinationType) {
      this.destinationType = destinationType;
    }

    /**
     * Gets the flag that indicates whether the topic subscriber is durable or not.
     * @return Durable true of false.
     */
    public Boolean isDurable() {
      return durable;
    }

    /**
     * Sets the flag indicating if the topic subscriber is durable or not.
     * @param durable True of false.
     */
    public void setDurable(final Boolean durable) {
      this.durable = durable;
    }

    /**
     * Gets the name of the durable.
     * @return String with the name of the durable.
     */
    public String getDurablename() {
      return durablename;
    }

    /**
     * Sets the name of the durable.
     * @param durablename Name of the durable.
     */
    public void setDurablename(final String durablename) {
      this.durablename = durablename;
    }

    /**
     * Overrides the toString method.
     * @return String.
     */
    @Override
    public String toString() {
      return String.format("Connecting to %s at %s, using username %s and destination %s.",
          type, url, username, destination);
    }

    /**
     * Queue or connection factory to use.
     */
    public String getFactory() {
      return factory;
    }

    /**
     * Sets the connection factory.
     * @param factory Connection factory.
     */
    public void setFactory(final String factory) {
      this.factory = factory;
    }

    /**
     * Flag that indicates whether ssl is enabled for this connection or not.
     */
    public Boolean isSsl() {

      if (ssl == null) {
        return false;
      }

      return ssl;
    }

    /**
     * Sets if the connection is ssl based or not.
     * @param ssl true or false.
     */
    public void setSsl(final Boolean ssl) {
      this.ssl = ssl;
    }

    /**
     * Path to the trusted certificates.
     */
    public String getTrustedcertificates() {
      if (trustedcertificates != null) {
        return trustedcertificates;
      } else {
        return "";
      }
    }

    /**
     * Sets the path to the trusted certificates.
     * @param trustedcertificates Path to the trusted certificates.
     */
    public void setTrustedcertificates(final String trustedcertificates) {
      this.trustedcertificates = trustedcertificates;
    }

    /**
     * Flag which indicates if the host validation should be done.
     */
    public Boolean isSslhost() {
      if (sslhost == null) {
        return false;
      }

      return sslhost;
    }

    /**
     * Sets if the ssl host should be validated.
     * @param sslhost true or false.
     */
    public void setSslhost(final Boolean sslhost) {
      this.sslhost = sslhost;
    }

    /**
     * Flag which indicates the ssl verification of the hostname should be validated.
     */
    public Boolean getSslhostname() {
      if (sslhostname == null) {
        sslhostname = false;
      }

      return sslhostname;
    }

    /**
     * Sets if the ssl hostname should be validated or not.
     * @param sslhostname sslhostname.
     */
    public void setSslhostname(final Boolean sslhostname) {
      this.sslhostname = sslhostname;
    }

    /**
     * Enables or disables the ssl trace.
     */
    public Boolean getSsltrace() {
      if (ssltrace == null) {
        return false;
      }

      return ssltrace;
    }

    /**
     * Sets if ssl trace is enabled.
     * @param ssltrace true or false.
     */
    public void setSsltrace(final Boolean ssltrace) {
      this.ssltrace = ssltrace;
    }

    /**
     * Flag which indicates that the ssl trace should be in debug mode.
     */
    public Boolean getSsldebugtrace() {
      if (ssldebugtrace == null) {
        return false;
      }

      return ssldebugtrace;
    }

    /**
     * Sets if ssl debug trace is enabled.
     * @param ssldebugtrace true or false.
     */
    public void setSsldebugtrace(final Boolean ssldebugtrace) {
      this.ssldebugtrace = ssldebugtrace;
    }

    /**
     * Gets the ssl cipher.
     * @return SSL cipher.
     */
    public String getSslcipher() {
      return sslcipher;
    }

    /**
     * Sets the ssl cipher to use.
     * @param sslcipher Ssl cipher.
     */
    public void setSslcipher(final String sslcipher) {
      this.sslcipher = sslcipher;
    }

    /**
     * Path to the SSL certificates to use.
     */
    public String getSslkeystore() {
      if (sslkeystore != null) {
        return sslkeystore;
      } else {
        return "";
      }
    }

    /**
     * Set location of ssl keystore.
     * @param sslkeystore Path to the ssl keystore.
     */
    public void setSslkeystore(final String sslkeystore) {
      this.sslkeystore = sslkeystore;
    }

    /**
     * Password for the ssl keystore.
     */
    public String getSslkeystorepassword() {
      return sslkeystorepassword != null ? sslkeystorepassword : "";
    }

    /**
     * Sets the ssl keystore password.
     * @param sslkeystorepassword Keystore password.
     */
    public void setSslkeystorepassword(final String sslkeystorepassword) {
      this.sslkeystorepassword = sslkeystorepassword;
    }

    /**
     * Gets the threshold above which the messages will be dropped and not processed. If 0 will not be applied.
     *
     * @return Message size threshold.
     */
    public long getMessageSizeThreshold() {
      return messageSizeThreshold;
    }

    /**
     * Sets the message size threshold.
     *
     * @param messageSizeThreshold Message size threshold.
     */
    public void setMessageSizeThreshold(final long messageSizeThreshold) {
      this.messageSizeThreshold = messageSizeThreshold;
    }

    /**
     * Whether to dump the dropped messages into the filesystem.
     *
     * @return Boolean whether to dump the messages into the filesystem.
     */
    public boolean isDumpDroppedMessages() {
      return dumpDroppedMessages;
    }

    /**
     * Sets whether to dump the dropped messages into the filesystem.
     *
     * @param dumpDroppedMessages Boolean whether to dump the dropped messages or not.
     */
    public void setDumpDroppedMessages(final boolean dumpDroppedMessages) {
      this.dumpDroppedMessages = dumpDroppedMessages;
    }

    /**
     * Path where to dump the dropped messages.
     *
     * @return Path where to dump the dropped messages.
     */
    public String getDumpedMessagesPath() {
      return dumpedMessagesPath;
    }

    /**
     * Path where to dump the dropped messages.
     *
     * @param dumpedMessagesPath Path where to dump the dropped messages.
     */
    public void setDumpedMessagesPath(final String dumpedMessagesPath) {
      this.dumpedMessagesPath = dumpedMessagesPath;
    }
  }

  /**
   * Validates the supplied configuration.
   */
  @PostConstruct
  private void validateConfiguration() throws InvalidConfigurationException {

    if (LOGGER.isInfoEnabled()) {
      LOGGER.info("Validating the read configuration...");
    }

    if (copy.isEmpty()) {
      throw new InvalidConfigurationException("A copy configuration needs to be supplied.");
    }

    for (final Copy checkCopy : copy) {
      validateCopy(checkCopy);
    }

    if (LOGGER.isInfoEnabled()) {
      LOGGER.info("Configuration is valid.");
    }
  }

  /**
   * Validates a given copy configuration.
   *
   * @param copy Copy to be validated.
   */
  private void validateCopy(final Copy copy) throws InvalidConfigurationException {
    if (copy.getName() == null || copy.getName().isEmpty()) {
      throw new InvalidConfigurationException("A name for the copy is necessary.");
    }

    if (copy.getSource() == null) {
      throw new InvalidConfigurationException("A source must be specified.");
    }

    if (copy.getTarget() == null) {
      throw new InvalidConfigurationException("A target must be specified.");
    }
  }
}
