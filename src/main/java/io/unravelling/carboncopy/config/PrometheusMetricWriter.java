/*
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * Contributors:
 * Tiago Veiga Lázaro
 */

package io.unravelling.carboncopy.config;

import io.prometheus.client.CollectorRegistry;
import io.prometheus.client.Counter;
import io.prometheus.client.Gauge;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.metrics.Metric;
import org.springframework.boot.actuate.metrics.writer.Delta;
import org.springframework.boot.actuate.metrics.writer.MetricWriter;
import org.springframework.stereotype.Component;

/**
 * Class that implements the different collectors we'll use to monitor CarbonCopy.
 */
@Component
public class PrometheusMetricWriter implements MetricWriter {

  private CollectorRegistry registry;

  private final ConcurrentMap<String, Counter> counters = new ConcurrentHashMap<>();
  private final ConcurrentHashMap<String, Gauge> gauges = new ConcurrentHashMap<>();

  @Autowired
  public PrometheusMetricWriter(final CollectorRegistry registry) {
    this.registry = registry;
  }

  /**
   * Increment the value of a metric (or decrement if the delta is negative). The name of the delta
   * is the name of the metric to increment.
   *
   * @param delta the amount to increment by
   */
  @Override
  public void increment(final Delta<?> delta) {
    counter(delta.getName()).inc(delta.getValue().doubleValue());
  }

  /**
   * Reset the value of a metric, usually to zero value. Implementations can discard the old values
   * if desired, but may choose not to. This operation is optional (some implementations may not be
   * able to fulfill the contract, in which case they should simply do nothing).
   *
   * @param metricName the name to reset
   */
  @Override
  public void reset(final String metricName) {
    counter(metricName).clear();
  }

  /**
   * Set the value of a metric.
   *
   * @param value the value
   */
  @Override
  public void set(final Metric<?> value) {
    gauge(value.getName()).set(value.getValue().doubleValue());
  }

  /**
   * Gets a counter.
   * @param name Name of the counter to fetch.
   * @return Counter.
   */
  private Counter counter(final String name) {
    final String key = sanitizeName(name);
    return counters.computeIfAbsent(key, k -> Counter.build().name(k).help(k).register(registry));
  }

  /**
   * Gets a gauge from the stored ones.
   * @param name Name of the gauge to obtain.
   * @return Gauge.
   */
  private Gauge gauge(final String name) {
    final String key = sanitizeName(name);
    return gauges.computeIfAbsent(key, k -> Gauge.build().name(k).help(k).register(registry));
  }

  /**
   * Cleans up the metric name so that if abides Prometheus formatting.
   * @param name Name of the metric.
   * @return Sanitized name.
   */
  private String sanitizeName(final String name) {
    return name.replaceAll("[^a-zA-Z0-9_]", "_");
  }
}
