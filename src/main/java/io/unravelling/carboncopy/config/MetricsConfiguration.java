/*
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * Contributors:
 * Tiago Veiga Lázaro
 */

package io.unravelling.carboncopy.config;

import com.codahale.metrics.ConsoleReporter;
import com.codahale.metrics.MetricRegistry;
import com.codahale.metrics.health.HealthCheckRegistry;
import com.ryantenney.metrics.spring.config.annotation.EnableMetrics;
import com.ryantenney.metrics.spring.config.annotation.MetricsConfigurerAdapter;

import java.util.concurrent.TimeUnit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Configures the metric collection features of the application.
 * @author Tiago Veiga Lázaro
 * @version 1.0.0
 */
@Configuration
@EnableMetrics
public class MetricsConfiguration extends MetricsConfigurerAdapter {

  /**
   * Metric registry to use.
   */
  private final MetricRegistry metricRegistry = new MetricRegistry();

  /**
   * Health check registry.
   */
  private final HealthCheckRegistry healthCheckRegistry = new HealthCheckRegistry();

  /**
   * Gets the metric registry.
   * @return MetricRegistry.
   */
  @Override
  @Bean
  public MetricRegistry getMetricRegistry() {
    return metricRegistry;
  }

  /**
   * Gets the health check registry.
   * @return HealthCheckRegistry.
   */
  @Override
  @Bean
  public HealthCheckRegistry getHealthCheckRegistry() {
    return healthCheckRegistry;
  }

  /**
   * Configures the reporters.
   * @param metricRegistry Metric registry to configure.
   */
  @Override
  public void configureReporters(final MetricRegistry metricRegistry) {
    // registerReporter allows the MetricsConfigurerAdapter to
    // shut down the reporter when the Spring context is closed
    final ConsoleReporter consoleReporter = ConsoleReporter
        .forRegistry(metricRegistry)
        .build();

    registerReporter(consoleReporter)
        .start(1, TimeUnit.MINUTES);
  }
}
