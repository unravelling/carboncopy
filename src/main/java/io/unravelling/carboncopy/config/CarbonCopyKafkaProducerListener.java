package io.unravelling.carboncopy.config;

import io.unravelling.carboncopy.utils.FileUtils;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.springframework.kafka.support.ProducerListener;

import java.nio.charset.StandardCharsets;

/**
 * #{PACKAGE_NAME}
 *
 * @author Tiago Veiga Lázaro
 * @since
 */
public class CarbonCopyKafkaProducerListener<k,v> implements ProducerListener<String, String> {

  final CarbonCopyConfiguration.ConnectionConfiguration configuration;

  public CarbonCopyKafkaProducerListener(final CarbonCopyConfiguration.ConnectionConfiguration connectionConfiguration) {
    this.configuration = connectionConfiguration;
  }

  @Override
  public void onSuccess(String topic, Integer partition, String key, String value, RecordMetadata recordMetadata) {
    // do nothing
  }

  @Override
  public void onError(String topic, Integer partition, String key, String value, Exception exception) {
    if (configuration.isDumpDroppedMessages()
        && value.getBytes(StandardCharsets.UTF_8).length > configuration.getMessageSizeThreshold()) {
      FileUtils.writeStringFile(configuration.getDumpedMessagesPath(), key, value);
    }
  }

  @Override
  public boolean isInterestedInSuccess() {
    return false;
  }
}
