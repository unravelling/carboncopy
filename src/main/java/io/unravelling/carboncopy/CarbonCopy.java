/*
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * Contributors:
 * Tiago Veiga Lázaro
 */

package io.unravelling.carboncopy;

import io.prometheus.client.Counter;
import io.unravelling.carboncopy.config.CarbonCopyConfiguration;
import io.unravelling.carboncopy.factories.producers.ProducerFactory;
import io.unravelling.carboncopy.factories.workers.WorkerFactory;
import io.unravelling.carboncopy.producers.CarbonProducer;
import io.unravelling.carboncopy.workers.CarbonWorker;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * Implementation of the CarbonCopy application.
 *
 * @author Tiago Veiga Lázaro
 * @version 1.0.0
 * @since 1.0.0
 */
@Component
public class CarbonCopy {

  /**
   * Class logger.
   */
  private static final Logger log = LoggerFactory.getLogger(CarbonCopy.class);

  /**
   * Configuration of the CarbonCopy.
   */
  private CarbonCopyConfiguration carbonCopyConfiguration;

  /**
   * List with all the created instances.
   */
  private static List<CarbonWorker> workers = new ArrayList<>();

  /**
   * Counter that keeps track of received messages.
   */
  public static final Counter receivedMessages = Counter.build()
      .name("received_messages")
      .help("Registers the received messages.")
      .labelNames("from", "destination_type", "destination_name")
      .register();

  /**
   * Counter that keeps track of successfully sent messages.
   */
  public static final Counter sentMessages = Counter.build()
      .name("sent_messages")
      .help("Registers the sent messages.")
      .labelNames("to", "destination_type", "destination_name")
      .register();

  public static final Counter droppedMessages = Counter.build()
      .name("dropped_messages")
      .help("Registers the dropped messages")
      .labelNames("from", "destination_type", "destination_name")
      .register();

  /**
   * Configuration of the carbon copy configuration to apply.
   *
   * @param carbonCopyConfiguration Configuration to apply.
   */
  public CarbonCopy(final CarbonCopyConfiguration carbonCopyConfiguration) {
    this.carbonCopyConfiguration = carbonCopyConfiguration;
  }

  /**
   * Method called to start processing.
   */
  @PostConstruct
  public void run() {
    printConfiguration();

    carbonCopyConfiguration.getCopy().forEach(config -> {
      try {

        // Builds the producer where to send the messages.
        final CarbonProducer producer = ProducerFactory.buildProducer(config.getTarget());

        // Creates the worker that will receive the messages and process them.
        final CarbonWorker worker = WorkerFactory.buildWorker(carbonCopyConfiguration, config,
            producer);
        workers.add(worker);

      } catch (Exception exception) {
        log.error(exception.getMessage(), exception);
      }
    });

  }

  /**
   * Prints the running configuration.
   */
  private void printConfiguration() {
    log.info("Carbon copy running configuration: ");

    carbonCopyConfiguration.getCopy().forEach(copy -> {
      log.info("Loaded copy {}: {}.", copy.getName(), copy.getDescription());
      log.info("Source: {}", copy.getSource());
      log.info("Target: {}", copy.getTarget());
    });
  }

  /**
   * Method to clean up pending resources.
   */
  @SuppressWarnings("unused")
  @PreDestroy
  private static void onClose() {
    log.info("Cleaning up Carbon Copy's pending resources.");

    for (final CarbonWorker worker: CarbonCopy.workers) {
      worker.close();
    }

    log.info("Done.");
  }
}
