package io.unravelling.carboncopy.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.FileSystem;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * File utilities.
 *
 * @author Tiago Veiga Lázaro
 * @version 2.5.0
 * @since 2.5.0
 */
public abstract class FileUtils {

  private static Logger log = LoggerFactory.getLogger(FileUtils.class);

  private static DateFormat df = new SimpleDateFormat("yyyy.MM.dd");

  private FileUtils() {}

  public static void writeStringFile(final String basePath, final String filename, final String content) {
    Date date = new Date();

    final Path path = Paths.get(basePath != null && ! basePath.isEmpty() ? basePath : ".", df.format(date));
    checkPath(path);

    try {
      Files.write(path.resolve(filename), content.getBytes(StandardCharsets.UTF_8));
    } catch (IOException exception) {
      log.error("Error while writing file...");
    }
  }

  public static void checkPath(final Path path) {
    if (! path.toFile().isDirectory()) {
      try {
        Files.createDirectory(path);
      } catch (IOException exception) {
        log.error("Exception occurred while trying to create path.");
      }
    }
  }
}
