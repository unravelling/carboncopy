/*
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * Contributors:
 * Tiago Veiga Lázaro
 */

package io.unravelling.carboncopy.factories.producers;

import io.unravelling.carboncopy.config.CarbonCopyConfiguration.ConnectionConfiguration;
import io.unravelling.carboncopy.exception.InvalidConfigurationException;
import io.unravelling.carboncopy.factories.connections.EmsConnectionFactory;
import io.unravelling.carboncopy.factories.connections.MqConnectionFactory;
import io.unravelling.carboncopy.producers.CarbonProducer;
import io.unravelling.carboncopy.producers.ConsoleProducer;
import io.unravelling.carboncopy.producers.EmsProducer;
import io.unravelling.carboncopy.producers.FileProducer;
import io.unravelling.carboncopy.producers.KafkaProducer;
import io.unravelling.carboncopy.producers.MqProducer;

import javax.jms.ConnectionFactory;

/**
 * <p>Builds producers, whom are able to send messages into a given target.</p>
 *
 * <p>Created on 2017/04/24</p>
 * @author Tiago Veiga Lázaro
 * @version 1.0.0
 */
public class ProducerFactory {

  /**
   * Private constructor to disable the instantiation of this class.
   */
  private ProducerFactory() {}

  /**
   * Builds a producer. This is the public method which should be called from the outside.
   * @param configuration Configuration of the producer to be created.
   * @return CarbonProducer the created producer.
   */
  public static CarbonProducer buildProducer(final ConnectionConfiguration configuration)
      throws Exception {
    CarbonProducer producer;

    switch (configuration.getType().toUpperCase()) {
      case "EMS":
        producer = buildEmsProducer(configuration);
        break;
      case "FILE":
        producer = buildFileProducer(configuration);
        break;
      case "MQ":
        producer = buildMqProducer(configuration);
        break;
      case "KAFKA":
        producer = buildKafkaProducer(configuration);
        break;
      default:
        producer = new ConsoleProducer();
    }

    return producer;
  }

  /**
   * Creates an EMS producer.
   * @param configuration Configuration of the producer.
   * @return CarbonProducer producer of EMS messages.
   */
  private static CarbonProducer buildEmsProducer(final ConnectionConfiguration configuration)
      throws InvalidConfigurationException {

    final ConnectionFactory factory =
        EmsConnectionFactory.buildConnectionFactory(configuration);

    return new EmsProducer(factory, configuration);
  }

  /**
   * Creates a MQ producer.
   * @param configuration Configuration of the producer.
   * @return CarbonProducer producer of MQ messages.
   */
  private static CarbonProducer buildMqProducer(final ConnectionConfiguration configuration)
      throws InvalidConfigurationException {

    final ConnectionFactory factory =
        MqConnectionFactory.buildConnectionFactory(configuration);

    return new MqProducer(factory, configuration);
  }

  /**
   * Creates a file producer.
   * @param configuration Configuration for the file producer.
   * @return Created producer.
   */
  private static CarbonProducer buildFileProducer(final ConnectionConfiguration configuration) {
    return new FileProducer(configuration);
  }

  /**
   * Creates a new Kafka producer.
   *
   * @param configuration Configuration from which to create the producer.
   * @return CarbonProducer.
   */
  private static CarbonProducer buildKafkaProducer(final ConnectionConfiguration configuration) {
    return new KafkaProducer(configuration);
  }
}
