package io.unravelling.carboncopy.factories.connections;

import io.unravelling.carboncopy.config.CarbonCopyConfiguration.ConnectionConfiguration;
import io.unravelling.carboncopy.config.CarbonCopyConfiguration.Copy;

import java.util.HashMap;
import java.util.Map;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.StringDeserializer;

import org.apache.kafka.common.serialization.StringSerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.boot.autoconfigure.kafka.ConcurrentKafkaListenerContainerFactoryConfigurer;
import org.springframework.context.annotation.Bean;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaProducerFactory;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.core.ProducerFactory;
import org.springframework.kafka.listener.ConcurrentMessageListenerContainer;
import org.springframework.kafka.listener.SeekToCurrentErrorHandler;
import org.springframework.kafka.listener.config.ContainerProperties;

/**
 * Holds the needed connection factories for Kafka support.
 *
 * @author Tiago Veiga Lázaro
 * @version 1.4.0
 * @since 1.4.0
 */
public abstract class KafkaConnectionFactory {

  /**
   * Default number of threads.
   */
  private static final int DEFAULT_THREADS = 5;

  /**
   * Default group name.
   */
  private static final String DEFAULT_GROUP = "carboncopy";

  /**
   * Class logger.
   */
  private static final Logger log = LoggerFactory.getLogger(KafkaConnectionFactory.class);

  /**
   * Default private constructor to disable class instantiation.
   */
  private KafkaConnectionFactory() {}

  /**
   * Builds a listener container.
   *
   * @param copyConfiguration Configuration.
   * @param threads Number of threads, as per configuration.
   * @param listener Listener for the container.
   * @return ConcurrentMessageListenerContainer.
   */
  public static ConcurrentMessageListenerContainer listenerContainerFromConfiguration(
      final Copy copyConfiguration, final int threads, final Object listener
  ) {

    final ConsumerFactory<String, String> consumerFactory =
        getConsumerFactory(copyConfiguration.getSource());

    final ContainerProperties containerProps = getConsumerContainerProperties(copyConfiguration);
    containerProps.setMessageListener(listener);

    ConcurrentMessageListenerContainer<String, String> container =
        new ConcurrentMessageListenerContainer<>(consumerFactory, containerProps);
    container.setConcurrency(threads > 0 ? threads : DEFAULT_THREADS);

    if (log.isDebugEnabled()) {
      log.debug("Using {} threads.", threads > 0 ? threads : DEFAULT_THREADS);
    }

    return container;
  }

  /**
   * Gets a new Kafka Template, which can be used to send messages.
   *
   * @param connectionConfiguration Configuration of the underlying connection.
   * @return KafkaTemplate.
   */
  public static KafkaTemplate<String, String> getKafkaTemplate(
      final ConnectionConfiguration connectionConfiguration
  ) {
    final ProducerFactory<String, String> producerFactory =
        getProducerFactory(connectionConfiguration);

    return new KafkaTemplate<>(producerFactory);
  }

//  @Bean
//  public ConcurrentKafkaListenerContainerFactory kafkaListenerContainerFactory(
//      ConcurrentKafkaListenerContainerFactoryConfigurer configurer,
//      ConsumerFactory<Object, Object> kafkaConsumerFactory,
//      KafkaTemplate<Object, Object> template) {
//    ConcurrentKafkaListenerContainerFactory<Object, Object> factory = new ConcurrentKafkaListenerContainerFactory<>();
//    configurer.configure(factory, kafkaConsumerFactory);
//    factory.setErrorHandler(new SeekToCurrentErrorHandler(
//        new DeadLetterPublishingRecoverer(template), 3));
//    return factory;
//  }

  /**
   * Creates a consumer factory from configuration.
   *
   * @param connectionConfiguration Configuration of the connection.
   * @return ConsumerFactory.
   */
  private static ConsumerFactory<String, String> getConsumerFactory(
      final ConnectionConfiguration connectionConfiguration) {

    final Map<String, Object> consumerConfig = getConsumerConfig(connectionConfiguration);

    return new DefaultKafkaConsumerFactory<>(consumerConfig);
  }

  /**
   * Gets a new producer factory.
   *
   * @param connectionConfiguration Connection configuration.
   * @return New producer factory.
   */
  private static ProducerFactory<String, String> getProducerFactory(
      final ConnectionConfiguration connectionConfiguration) {

    final Map<String, Object> producerConfig = getProducerConfig(connectionConfiguration);

    return new DefaultKafkaProducerFactory<>(producerConfig);
  }

  /**
   * Gets the container properties.
   *
   * @param copyConfiguration Configuration.
   * @return ContainerProperties.
   */
  private static ContainerProperties getConsumerContainerProperties(final Copy copyConfiguration) {
    return new ContainerProperties(copyConfiguration.getSource().getDestination());
  }

  /**
   * Gets the common connection properties.
   *
   * @param connectionConfig Connection configuration from which to setup the connection.
   * @return Map with the common configurations of the connection.
   */
  private static Map<String, Object> getConnectionProperties(
      final ConnectionConfiguration connectionConfig) {

    final Map<String, Object> connectionProperties = new HashMap<>();
    connectionProperties.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, connectionConfig.getUrl());
    return connectionProperties;
  }

  /**
   * Gets the consumer connection configurations.
   *
   * @param connectionConfiguration Connection configuration.
   * @return Map with the settings of the configuration.
   */
  private static Map<String, Object> getConsumerConfig(
      final ConnectionConfiguration connectionConfiguration) {

    final Map<String, Object> consumerConfig = getConnectionProperties(connectionConfiguration);
    consumerConfig.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
    consumerConfig.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);

    final String consumer = connectionConfiguration.getConsumerName();
    consumerConfig.put(ConsumerConfig.GROUP_ID_CONFIG,
        consumer == null || consumer.isEmpty() ? DEFAULT_GROUP : consumer);

    return consumerConfig;
  }

  /**
   * Gets the producer connection configurations.
   *
   * @param connectionConfiguration Connection configuration.
   * @return Map with the settings of the producer.
   */
  private static Map<String, Object> getProducerConfig(
      final ConnectionConfiguration connectionConfiguration) {

    final Map<String, Object> consumerConfig = getConnectionProperties(connectionConfiguration);
    consumerConfig.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
    consumerConfig.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class);

    return consumerConfig;
  }
}
