/*
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * Contributors:
 * Tiago Veiga Lázaro
 */

package io.unravelling.carboncopy.factories.connections;

import com.ibm.mq.jms.MQConnectionFactory;
import com.ibm.mq.jms.MQQueueConnectionFactory;
import com.ibm.msg.client.wmq.WMQConstants;

import io.unravelling.carboncopy.config.CarbonCopyConfiguration;
import io.unravelling.carboncopy.exception.InvalidConfigurationException;

import javax.jms.ConnectionFactory;
import javax.jms.JMSException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jms.connection.UserCredentialsConnectionFactoryAdapter;

/**
 * Class that implements the IBM MQ Connection factories capabilities.
 */
public final class MqConnectionFactory {
  /**
   * Class logger.
   */
  private static final Logger log = LoggerFactory.getLogger(MqConnectionFactory.class);

  /**
   * Private constructor to disable instantiation of this class.
   */
  private MqConnectionFactory() {}

  /**
   * Builds a ConnectionFactory to connect to a target Tibco EMS server.
   *
   * @param connectionConfiguration Configuration of the connection to be built.
   * @return ConnectionFactory.
   */
  @SuppressWarnings("WeakerAccess")
  public static ConnectionFactory buildConnectionFactory(
      final CarbonCopyConfiguration.ConnectionConfiguration connectionConfiguration)
      throws InvalidConfigurationException {

    if (log.isInfoEnabled()) {
      log.debug("Creating IBM MQ ConnectionFactory to {}.", connectionConfiguration.getUrl());
    }

    final ConnectionFactory connectionFactory =
        MqConnectionFactory.buildNativeConnectionFactory(connectionConfiguration);

    final UserCredentialsConnectionFactoryAdapter factory =
        new UserCredentialsConnectionFactoryAdapter();

    factory.setTargetConnectionFactory(connectionFactory);
    factory.setUsername(connectionConfiguration.getUsername());
    factory.setPassword(connectionConfiguration.getPassword());

    if (log.isDebugEnabled()) {
      log.debug("Connection factory created.");
    }

    return factory;
  }

  /**
   * Builds a native Tibco EMS connection factory based on the type of the destination.
   * @param connectionConfiguration Connection configuration.
   * @return Factory.
   */
  private static MQConnectionFactory buildNativeConnectionFactory(
      final CarbonCopyConfiguration.ConnectionConfiguration connectionConfiguration)
      throws InvalidConfigurationException {

    MQConnectionFactory factory;

    try {

      final String url = connectionConfiguration.getUrl();
      final String hostname = url.substring(0, url.indexOf(':'));
      final int port = Integer.parseInt(url.substring(url.indexOf(':') + 1, url.indexOf('/')));
      final String channel = url.substring(url.indexOf('/') + 1);

      factory = new MQQueueConnectionFactory();
      factory.setHostName(hostname);
      factory.setPort(port);
      factory.setChannel(channel);
      factory.setQueueManager(connectionConfiguration.getFactory());
      factory.setTransportType(WMQConstants.WMQ_CM_CLIENT);

      if (connectionConfiguration.isSsl()) {

        factory.setSSLCipherSuite(connectionConfiguration.getSslcipher());

        if (log.isInfoEnabled()) {
          log.info("Using SSL chipher: {}", connectionConfiguration.getSslcipher());
          log.info("Connecting to {} on port {} and channel {}.", hostname,
              port, channel);
        }
      }

      return factory;

    } catch (final JMSException jmsException) {
      log.error("Configuration supplied did not allow for a correct configuration.", jmsException);
      throw new InvalidConfigurationException(jmsException.getMessage());
    }
  }
}
