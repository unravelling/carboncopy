/*
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * Contributors:
 * Tiago Veiga Lázaro
 */

package io.unravelling.carboncopy.factories.connections;

import static com.tibco.tibjms.TibjmsSSL.addTrustedCerts;

import com.tibco.tibjms.TibjmsConnectionFactory;
import com.tibco.tibjms.TibjmsQueueConnectionFactory;
import com.tibco.tibjms.TibjmsTopicConnectionFactory;

import io.unravelling.carboncopy.config.CarbonCopyConfiguration;
import io.unravelling.carboncopy.config.CarbonCopyConfiguration.ConnectionConfiguration;
import io.unravelling.carboncopy.exception.InvalidConfigurationException;

import java.util.Properties;

import javax.jms.ConnectionFactory;
import javax.jms.JMSSecurityException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jms.connection.UserCredentialsConnectionFactoryAdapter;

/**
 * Class that implements the EMS Connection factories capabilities.
 */
public class EmsConnectionFactory {
  /**
   * Class logger.
   */
  private static final Logger log = LoggerFactory.getLogger(EmsConnectionFactory.class);

  /**
   * True string for use in configurations.
   */
  private static final String FALSE = "false";

  /**
   * Private constructor to prevent the instantiation of this class.
   */
  private EmsConnectionFactory() {}

  /**
   * Builds a ConnectionFactory to connect to a target Tibco EMS server.
   *
   * @param connectionConfiguration Configuration of the connection to be built.
   * @return ConnectionFactory.
   */
  public static ConnectionFactory buildConnectionFactory(
      final CarbonCopyConfiguration.ConnectionConfiguration connectionConfiguration)
      throws InvalidConfigurationException {

    if (log.isDebugEnabled()) {
      log.debug("Creating EMS ConnectionFactory to {}.", connectionConfiguration.getUrl());
    }

    final TibjmsConnectionFactory connectionFactory =
        EmsConnectionFactory.buildNativeFactory(connectionConfiguration);

    final UserCredentialsConnectionFactoryAdapter factory =
        new UserCredentialsConnectionFactoryAdapter();

    factory.setTargetConnectionFactory(connectionFactory);
    factory.setUsername(connectionConfiguration.getUsername());
    factory.setPassword(connectionConfiguration.getPassword());

    if (log.isDebugEnabled()) {
      log.debug("Connection factory created.");
    }

    return factory;
  }

  /**
   * Builds a native Tibco EMS connection factory based on the type of the destination.
   * @param connectionConfiguration Connection configuration.
   * @return Factory.
   */
  private static TibjmsConnectionFactory buildNativeFactory(
      final CarbonCopyConfiguration.ConnectionConfiguration connectionConfiguration)
      throws InvalidConfigurationException {

    TibjmsConnectionFactory factory;

    switch (connectionConfiguration.getDestinationType()) {
      case "queue":
        factory = buildQueueConnectionFactory(connectionConfiguration);
        break;
      case "topic":
        factory = buildTopicConnectionFactory(connectionConfiguration);
        break;
      default:
        throw new InvalidConfigurationException("Unsupported destination type.");
    }

    return factory;
  }

  /**
   * Builds a queue connection factory.
   *
   * @param connectionConfiguration Configurations to use for building the factory.
   * @return TibjmsQueueConnectionFactory.
   */
  private static TibjmsQueueConnectionFactory buildQueueConnectionFactory(
      final ConnectionConfiguration connectionConfiguration) {

    if (log.isInfoEnabled()) {
      log.info("Creating a queue connection factory connection to access {}.",
          connectionConfiguration.getUrl());
    }

    return new TibjmsQueueConnectionFactory(connectionConfiguration.getUrl(), null,
        buildEnvironmentProperties(connectionConfiguration));
  }

  /**
   * Builds a topic connection factory.
   *
   * @param connectionConfiguration Configurations to use for building the factory.
   * @return TibjmsQueueConnectionFactory.
   */
  private static TibjmsTopicConnectionFactory buildTopicConnectionFactory(
      final ConnectionConfiguration connectionConfiguration) {

    if (log.isInfoEnabled()) {
      log.info("Creating a topic connection factory connection to access {}.",
          connectionConfiguration.getUrl());
    }

    return new TibjmsTopicConnectionFactory(connectionConfiguration.getUrl(), null,
        buildEnvironmentProperties(connectionConfiguration));
  }

  /**
   * Builds a property container with the environment settings.
   * @return Properties.
   */
  private static Properties buildEnvironmentProperties(final ConnectionConfiguration config) {

    final Properties environment = new Properties();

    environment.put("context", "com.tibco.tibjms.naming.TibjmsInitialContextFactory");
    environment.put("factory", config.getFactory());
    environment.put("url", config.getUrl());
    environment.put("username", config.getUsername());
    environment.put("password", config.getPassword());

    if (config.isSsl()) {
      return configureSsl(environment, config);
    }

    return environment;
  }

  /**
   * Configures the properties of the ems connection to make use of ssl.
   *
   * @param environment Environment properties to use in the connection.
   * @param config Configuration settings of the connection.
   * @return Properties with SSL enabled.
   */
  private static Properties configureSsl(final Properties environment,
                                         final ConnectionConfiguration config) {

    environment.put("ssl", "true");

    environment.put("ssltrusted", config.getTrustedcertificates());

    try {
      addTrustedCerts(config.getTrustedcertificates());
    } catch (JMSSecurityException jmsSecurityException) {
      log.error("Error while adding the trusted certificates.", jmsSecurityException);
    }


    environment.put("noverifyhost", FALSE);
    environment.put("noverifyhostname", FALSE);
    environment.put("ssltrace", config.getSsltrace().toString());
    environment.put("ssldebugtrace", config.getSsldebugtrace().toString());

    return environment;
  }
}
