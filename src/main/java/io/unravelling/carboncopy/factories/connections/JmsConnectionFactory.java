/*
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * Contributors:
 * Tiago Veiga Lázaro
 */

package io.unravelling.carboncopy.factories.connections;

import io.unravelling.carboncopy.config.CarbonCopyConfiguration;
import io.unravelling.carboncopy.config.CarbonCopyConfiguration.ConnectionConfiguration;
import io.unravelling.carboncopy.exception.InvalidConfigurationException;

import javax.jms.ConnectionFactory;
import javax.jms.Session;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jms.listener.DefaultMessageListenerContainer;

/**
 * Builds a connection to a target.
 */
public class JmsConnectionFactory {

  /**
   * Class logger.
   */
  private static final Logger log = LoggerFactory.getLogger(JmsConnectionFactory.class);

  /**
   * Private constructor to disable instantiation of this class.
   */
  private JmsConnectionFactory() {
  }

  /**
   * Builds a ConnectionFactory to connect to a target Tibco EMS server.
   *
   * @param connectionConfiguration Configuration of the connection to be built.
   * @return ConnectionFactory.
   */
  static ConnectionFactory buildEmsConnectionFactory(
      final CarbonCopyConfiguration.ConnectionConfiguration connectionConfiguration)
      throws InvalidConfigurationException {

    return EmsConnectionFactory.buildConnectionFactory(connectionConfiguration);
  }

  /**
   * Builds an MQ connection factory.
   * @param connectionConfiguration Configuration of the connection.
   * @return Created connection factory.
   * @throws InvalidConfigurationException Exception occurred while creating the connection factory.
   */
  static ConnectionFactory buildMqConnectionFactory(
      final CarbonCopyConfiguration.ConnectionConfiguration connectionConfiguration)
      throws InvalidConfigurationException {

    return MqConnectionFactory.buildConnectionFactory(connectionConfiguration);
  }

  /**
   * Builds a message listener container.
   * @param configuration Configuration from which to create the listener.
   * @param factory Connection factory to use to create the listener.
   * @return Created DefaultMessageListenerContainer.
   */
  public static DefaultMessageListenerContainer buildMessageListenContainer(
      final ConnectionConfiguration configuration, final ConnectionFactory factory) {

    if (log.isInfoEnabled()) {
      log.info("Creating message {} listener.", configuration.getType());
    }

    final DefaultMessageListenerContainer container = new DefaultMessageListenerContainer();
    container.setConnectionFactory(factory);

    if ("topic".equalsIgnoreCase(configuration.getDestinationType())) {
      container.setPubSubDomain(true);
    }

    container.setDestinationName(configuration.getDestination());

    if ("topic".equals(configuration.getDestinationType())) {
      container.setConcurrency("1");
    } else {
      //TODO: Replace this with property setting in the configuration file.
      container.setConcurrency("5");
    }

    container.setSessionAcknowledgeMode(Session.CLIENT_ACKNOWLEDGE);
    container.setSessionTransacted(false);
    container.afterPropertiesSet();

    return container;
  }
}
