/*
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * Contributors:
 * Tiago Veiga Lázaro
 */

package io.unravelling.carboncopy.factories.workers;

import io.unravelling.carboncopy.config.CarbonCopyConfiguration;
import io.unravelling.carboncopy.config.CarbonCopyConfiguration.Copy;
import io.unravelling.carboncopy.exception.InvalidConfigurationException;
import io.unravelling.carboncopy.producers.CarbonProducer;
import io.unravelling.carboncopy.workers.CarbonWorker;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Builds workers for usage by Carbon Copy application.
 * @author Tiago Veiga Lazaro
 */
public abstract class WorkerFactory {

  /**
   * Class logger.
   */
  private static final Logger log = LoggerFactory.getLogger(WorkerFactory.class);

  /**
   * Private constructor to disable instantiation.
   */
  private WorkerFactory() {}

  /**
   * Builds a worker based on a specific configuration.
   * @param copyConfiguration Configuration of the copy to apply.
   * @return Worker.
   */
  @SuppressWarnings("squid:S00112")
  public static CarbonWorker buildWorker(
      final CarbonCopyConfiguration carbonCopyConfiguration,
      final Copy copyConfiguration,
      final CarbonProducer producer)
        throws Exception {

    CarbonWorker carbonWorker;

    if (log.isDebugEnabled()) {
      log.debug("Creating new worker...");
    }

    switch (copyConfiguration.getSource().getType().toUpperCase()) {
      case "EMS":
        carbonWorker = EmsWorkerFactory.buildTibcoEmsWorker(copyConfiguration);
        break;
      case "MQ":
        carbonWorker = MqWorkerFactory.buildMqWorker(copyConfiguration);
        break;
      case "KAFKA":
        carbonWorker = KafkaWorkerFactory.buildKafkaWorker(copyConfiguration,
            carbonCopyConfiguration, producer);
        break;
      default:
        throw new InvalidConfigurationException("Source type is not valid.");
    }

    carbonWorker.setProducer(producer);

    if (log.isDebugEnabled()) {
      log.debug("New worker created.");
    }

    return carbonWorker;
  }
}
