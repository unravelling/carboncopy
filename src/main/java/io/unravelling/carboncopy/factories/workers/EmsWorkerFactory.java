package io.unravelling.carboncopy.factories.workers;

import io.unravelling.carboncopy.config.CarbonCopyConfiguration;
import io.unravelling.carboncopy.config.CarbonCopyConfiguration.ConnectionConfiguration;
import io.unravelling.carboncopy.exception.InvalidConfigurationException;
import io.unravelling.carboncopy.factories.connections.EmsConnectionFactory;
import io.unravelling.carboncopy.factories.connections.JmsConnectionFactory;
import io.unravelling.carboncopy.workers.CarbonWorker;
import io.unravelling.carboncopy.workers.JmsWorker;

import javax.jms.ConnectionFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jms.listener.DefaultMessageListenerContainer;

/**
 * Provides the factory method to build a Tibco EMS based worker (listens on Ems queues for work).
 *
 * @author Tiago Veiga Lázaro
 * @version 1.4.0
 * @since 1.4.0
 */
@SuppressWarnings("WeakerAccess")
public abstract class EmsWorkerFactory {

  /**
   * Logger of the class.
   */
  private static final Logger log = LoggerFactory.getLogger(EmsWorkerFactory.class);

  /**
   * Private constructor to disable instantiation.
   */
  private EmsWorkerFactory() {}

  /**
   * Creates a Tibco Ems worker.
   * @param copyConfig Configuration to apply to the worker.
   * @return Worker that will copy the messages.
   */
  static CarbonWorker buildTibcoEmsWorker(final CarbonCopyConfiguration.Copy copyConfig)
      throws InvalidConfigurationException {

    if (log.isInfoEnabled()) {
      log.info("Creating a Tibco EMS worker...");
    }

    final ConnectionConfiguration sourceConfiguration = copyConfig.getSource();

    final ConnectionFactory connectionFactory =
        EmsConnectionFactory.buildConnectionFactory(sourceConfiguration);

    final DefaultMessageListenerContainer container =
        JmsConnectionFactory.buildMessageListenContainer(sourceConfiguration, connectionFactory);

    // Sets durable options.
    if ("topic".equalsIgnoreCase(sourceConfiguration.getDestinationType())
        && sourceConfiguration.isDurable()) {
      container.setSubscriptionDurable(true);

      final String durableName = sourceConfiguration.getDurablename();

      container.setDurableSubscriptionName(
          durableName != null && !durableName.isEmpty() ? durableName : "CarbonCopy"
      );
    }

    container.start();

    final CarbonWorker worker = new JmsWorker(copyConfig.getSource());
    container.setMessageListener(worker);

    if (log.isInfoEnabled()) {
      log.info("Jms listener is active: {}.", container.isActive());
      log.info("New worker for Tibco EMS source created.");
    }

    return worker;
  }
}
