package io.unravelling.carboncopy.factories.workers;

import io.unravelling.carboncopy.config.CarbonCopyConfiguration;
import io.unravelling.carboncopy.exception.InvalidConfigurationException;
import io.unravelling.carboncopy.factories.connections.JmsConnectionFactory;
import io.unravelling.carboncopy.factories.connections.MqConnectionFactory;
import io.unravelling.carboncopy.workers.CarbonWorker;
import io.unravelling.carboncopy.workers.JmsWorker;

import javax.jms.ConnectionFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jms.listener.DefaultMessageListenerContainer;

/**
 * Factory to build an MQ worker.
 *
 * @author Tiago Veiga Lázaro
 * @version 1.4.0
 * @since 1.4.0
 */
@SuppressWarnings("WeakerAccess")
public abstract class MqWorkerFactory {

  /**
   * Class logger.
   */
  private static final Logger log = LoggerFactory.getLogger(MqWorkerFactory.class);

  /**
   * Private constructor to disable instantiation.
   */
  private MqWorkerFactory() {}

  /**
   * Creates a Tibco Ems worker.
   * @param copyConfig Configuration to apply to the worker.
   * @return Worker that will copy the messages.
   */
  static CarbonWorker buildMqWorker(final CarbonCopyConfiguration.Copy copyConfig)
      throws InvalidConfigurationException {

    if (log.isInfoEnabled()) {
      log.info("Creating an IBM MQ worker...");
    }

    CarbonCopyConfiguration.ConnectionConfiguration sourceConfiguration = copyConfig.getSource();

    final ConnectionFactory connectionFactory =
        MqConnectionFactory.buildConnectionFactory(sourceConfiguration);

    final DefaultMessageListenerContainer container =
        JmsConnectionFactory.buildMessageListenContainer(sourceConfiguration, connectionFactory);

    container.start();

    final CarbonWorker worker = new JmsWorker(copyConfig.getSource());
    container.setMessageListener(worker);

    if (log.isInfoEnabled()) {
      log.info("Jms listener is active: {}.", container.isActive());
      log.info("New worker for IBM MQ source created.");
    }

    return worker;
  }
}
