package io.unravelling.carboncopy.factories.workers;

import io.unravelling.carboncopy.config.CarbonCopyConfiguration;
import io.unravelling.carboncopy.config.CarbonCopyConfiguration.Copy;
import io.unravelling.carboncopy.factories.connections.KafkaConnectionFactory;
import io.unravelling.carboncopy.producers.CarbonProducer;
import io.unravelling.carboncopy.workers.CarbonWorker;
import io.unravelling.carboncopy.workers.KafkaWorker;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.kafka.listener.ConcurrentMessageListenerContainer;


/**
 * Class that implements the needed features to create a kafka worker.
 *
 * @author Tiago Veiga Lázaro
 * @version 2.4.0
 * @since 2.4.0
 */
public class KafkaWorkerFactory {

  /**
   * Class logger.
   */
  private static final Logger log = LoggerFactory.getLogger(KafkaWorkerFactory.class);

  /**
   * Builds a Kafka worker from configuration.
   *
   * @param copyConfiguration Configuration of the copy instance.
   * @param carbonCopyConfiguration CarbonCopy configuration applied.
   * @param producer Producer where to send the messages to.
   * @return CarbonWorker.
   */
  @SuppressWarnings("WeakerAccess")
  public static CarbonWorker buildKafkaWorker(
      final Copy copyConfiguration,
      final CarbonCopyConfiguration carbonCopyConfiguration,
      final CarbonProducer producer
  ) {

    if (log.isDebugEnabled()) {
      log.debug("Creating a new Kafka worker...");
    }

    final KafkaWorker<String, String> worker = new KafkaWorker<>(copyConfiguration.getSource());
    worker.setProducer(producer);

    final ConcurrentMessageListenerContainer container =
        KafkaConnectionFactory.listenerContainerFromConfiguration(copyConfiguration,
            carbonCopyConfiguration.getMinthreads(), worker);

    worker.setContainer(container);
    container.start();

    if (log.isDebugEnabled()) {
      log.debug("Kafka worker created.");
    }
    return worker;
  }
}
