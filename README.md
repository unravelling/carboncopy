# Carbon Copy   [![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)   [![Build Status](https://travis-ci.org/unravellingtechnologies/carboncopy.svg?branch=master)](https://travis-ci.org/unravellingtechnologies/carboncopy)   [![Codacy Badge](https://api.codacy.com/project/badge/Grade/108517ae71cc4460acbce18e6010e5bd)](https://www.codacy.com/app/unravellingtechnologies/carboncopy?utm_source=github.com&amp;utm_medium=referral&amp;utm_content=unravellingtechnologies/carboncopy&amp;utm_campaign=Badge_Grade)    

This project aims to provide a generic messaging bridging application, that is to move messages from one source to another. Supported systems at the moment are:
* Tibco EMS (queues and topics alike)
* IBM MQ (queues and topics supported) - Built with 
* Files
## Releases
Releases are automatically built via Travis CI and available through the Releases page.
## Building from source
First step is to clone the repository to a folder of your choice:
`git clone https://github.com/unravellingtechnologies/carboncopy.git\
cd carboncopy`
Then you can either package the application or run it directly with gradle.
To run it:
`./gradlew bootRun`
To build the file:
`./gradlew bootRepackage`
## Usage
You need to choose a profile in the file application.yml located in:
`src/main/resources/application.yml`

Then we need to have a file application-<profile name>.yml. For example:
`application-file.yml` will enable us to have a file with a file bridge configuration.
## Contributing
1. Fork it!
2. Create your feature branch: `git checkout -b my-new-feature`
3. Commit your changes: `git commit -am 'Add some feature'`
4. Push to the branch: `git push origin my-new-feature`
5. Submit a pull request :D
## History
1.0.0 Finished basic version with full Tibco EMS support and file producer.
## Credits
This project is based on the amazing Spring Boot.
## License
This software is distributed according to the MIT License.
